﻿# Библиотека тем для финальной десятой лабы
## Важна иформация о нашей библиотеке стилей:

Самое важно, что нужно знать о библиотеке - это то что в ней уже есть предзаготовленные стили для всех кнопок в приложении

## Авторы
### EtoNeAnanasbI95
[Telegram](https://t.me/BIG_zh0pa)
[Discord](https://discord.com/users/507229544013627392)
[GitHub](https://github.com/EtoNeAnanasbI95)

### BroGideon
[Telegram](https://t.me/bro_gideon)
[Discord](https://discord.com/users/1176876951756083280)
[GitHub](https://github.com/BroGideon)

***

## СТИЛИ СТИЛИ СТИЛИ БОЛЬШЕ СТИЛЕЙ


### Стили для кнопок

1. FocusingButton - синяя кнопка
2. AttentionButton - красная кнопка
3. ButtonBar - кнопка с прозрачным фоном

***

### Стили для других элементов интерфейса

1. ClearComboBoxStyle - нормальный комбобокс по макету 10 лабы
2. ClearDatePicker - нормальный дейтпикер по макету 10 лабы
3. ClearTextbox - нормальный текстбокс по макету 10 лабы
4. ClearToggleButton - нормальный тоглбаттон по макету 10 лабы
5. MenuComboBox - нормальный комбобокс для меню по макету 10 лабы

***

### Также есть предзаготовленные цвета

1. PrimaryBackground (Просто белый)
2. SecondaryBackground (Серый)
3. PrimaryForeground (Черный)
4. SecondaryForeground (Темно серый)
5. Focusing (Синий)
6. Attention (Красный)

***

### Еще есть предзаготовленный шрифт
1. Inter
